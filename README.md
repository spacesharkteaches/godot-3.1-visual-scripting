# Space Shark Teaches Godot 3.1 Visual Scripting

Welcome to the Space Shark Teaches Godot 3.1 Visual Scripting git repo!

This repository contains the most up-to-date version of the Godot 3.1 Visual Scripting tutorial.

Please see [Tags](https://gitlab.com/spacesharkteaches/godot-3.1-visual-scripting/-/tags) to the version of the project that best lines up with your place in the lessons.

### [Space Shark Studios on YouTube](https://www.youtube.com/c/SpaceSharkStudios)

### [Godot 3.1 Visual Script YouTube Playlist](https://www.youtube.com/playlist?list=PLxw4Pmjew9pzVgENhgnlzjjNgoRm89TEg)